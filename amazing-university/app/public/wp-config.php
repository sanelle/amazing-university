<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GmQwFOys6lqwVHMtBcp6vVCKZxRoDPhx3FQZnNwYyqqyUihoyT/nfxZhkJAixLYOwDCnGi9XwU1WrqKqQUv2gg==');
define('SECURE_AUTH_KEY',  'LDIyRGwEpmeVn5mSDrsmiQpbXOpUACeS4eChnIiRq9kvtqoQZmAShjtXDy7BToWDK8Azl6LvSVnM4+ukH4SPnA==');
define('LOGGED_IN_KEY',    'HO4rpAaRysZLsLsmFMXreO8UR5s+5SV3Y3xpZF37JqOD6sWW+fgixF5zm/00rLQuwaIlhegMKXRL59LAMMpfaQ==');
define('NONCE_KEY',        '0/qoSSyIYyErfnFkPHiG4pXZvfhZHTX9/fuQued31dBUf1elkgtF1Ha4BQfPo9ZlEFa6T5tWtjDhB2aQnQW1Ag==');
define('AUTH_SALT',        'v10NP98Ug8Yk6GxbzwqfNSfgRNKObGTFYgU7QqY0kFI1uYx2GlHFXjKZUzKiRM1876HWt1cXzW98q538To09uw==');
define('SECURE_AUTH_SALT', 'bMqrUq6r8B5szs/pFi5Q11kQpM8uxM8C/2j/tKCtclTZt9yisfqOvSPKh1Z8DfXF8po4rYk01jZ1XpkLEnS8Sw==');
define('LOGGED_IN_SALT',   '6PwCIneoZOOgScj3MiKTHbTK8PvK45h6imedeWbIAehaArIMH26R8tTg5wveWSl1ITjDwQGmoNqvSEM8qDBSlQ==');
define('NONCE_SALT',       'yFPLooTvhycdZyJMdrT0307RehuD/QD5aR0et6r3k1SfniTeg0jk0boZIy7jYq6RjQEotGlOWFKoNhu6JKY/yQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
