<?php   

get_header();

while(have_posts()) {
    the_post(); 
    pagebanner();
    ?>

  <div class="container container--narrow page-section">
  <div class="metabox metabox--position-up metabox--with-home-link">
      <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('campus'); ?>"><i class="fa fa-home" aria-hidden="true"></i> All Campuses </a> <span class="metabox__main"><?php the_title(); ?></span></p>
    </div>
  <div class="generic-content"><?php the_content(); ?></div>
  
  <div class="acf-map">

<?php 
$maplocation = get_field('map_location');
?>


<div class="marker" data-lat="<?php echo $maplocation['lat'] ?>" data-lng="<?php echo $maplocation['lng'] ?>">
<h3><?php the_title(); ?></h3>
 <?php echo $map_location['address']; ?>
</div>
</div>


  <?php

$relatedprograms = new WP_Query(array(
    'posts_per_page' => -1,
    'post_type' => 'program',
    'order_by' => 'title',
    'order' => 'ASC',
    'meta_query' => array(
      array(
      'key' => 'related_campus',
      'compare' => 'LIKE',
      'value' => '"' . get_the_ID() . '"'

    )
    )
  ));

  if ($relatedprograms->have_posts()) {
      echo '<hr class="section-break">';
      echo '<h2 class="headline headline--medium">Programs availabe at this campus</h2>';
       
      echo '<ul class="min-list link-list">';
      while($relatedprograms->have_posts()) {
        $relatedprograms->the_post(); ?>
        <li>
        <a href="<?php the_permalink(); ?>">
         
         <?php the_title(); ?>
        </a>
        </li>


      <?php }
     echo '<ul>';

  }


     wp_reset_postdata();
   


        $today = date('Ymd');
        $homepageevents = new WP_Query(array(
          'posts_per_page' => 2,
          'post_type' => 'event',
          'order_by' => 'meta_value_',
          'order' => 'ASC',
          'meta_query' => array(
            array(
              'key' => 'event_date',
              'compare' => '>=',
              'value' => $today,
              'type' => 'numeric'
            ),

          array(
            'key' => 'related_programs',
            'compare' => 'LIKE',
            'value' => '"' . get_the_ID() . '"'

          )
          )
        ));

        if ($homepageevents->have_posts()) {
            echo '<hr class="section-break">';
            echo '<h2 class="headline headline--medium">Upcoming ' . get_the_title() . ' Events</h2>';
    
            while($homepageevents->have_posts()) {
              $homepageevents->the_post(); ?>
    
    <div class="event-summary">
              <a class="event-summary__date t-center" href="<?php the_permalink(); ?>">
                <span class="event-summary__month"><?php 
                $eventDate = new DateTime(get_field('event-date'));
                echo $eventDate->format('M')
                 ?></span>
                <span class="event-summary__day"><?php echo $eventDate->format('d'); ?></span>  
              </a>
              <div class="event-summary__content">
                <h5 class="event-summary__title headline headline--tiny"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                <p><?php if (has_excerpt()) {
                 echo get_the_excerpt();
                } else {
                  echo wp_trim_words(get_the_content(), 18);
                } ?> <a href="<?php the_permalink(); ?>" class="nu gray">Learn more</a></p>
              </div>
            </div>
    
            <?php }

        }
     
        
        ?>

       

</div>


<?php }

get_footer();

?>