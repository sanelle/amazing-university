<?php   


get_header();

while(have_posts()) {
    the_post(); 
    pagebanner();
    ?>
   


  <div class="container container--narrow page-section">

  <?php
  $moreme = wp_get_post_parent_id(get_the_ID());
 if ($moreme) { ?>
 <div class="metabox metabox--position-up metabox--with-home-link">
      <p><a class="metabox__blog-home-link" href="<?php echo get_permalink($moreme); ?>"><i class="fa fa-home" aria-hidden="true"></i> Back to <?php echo get_the_title($moreme); ?></a> <span class="metabox__main"><?php the_title(); ?></span></p>
    </div>
 <?php }

 ?>


    
    <?php
    $array = get_pages(array(
      'child_of' => get_the_ID()
    ));
     if ($moreme or $array) { ?>
    <div class="page-links">
      <h2 class="page-links__title"><a href="<?php echo get_permalink($moreme); ?>"><?php echo get_the_title($moreme); ?></a></h2>
      <ul class="min-list">
     <?php 
     if ($moreme) {
       $more = $moreme;
     } else {
       $more = get_the_ID();
     }

     
     wp_list_pages(array(
       'title_li' => NULL,
       'child_of' =>  $more
     ));
     ?>
      </ul>
    </div>
    <?php } ?>


    <div class="generic-content">
    
    </div>

    <?php the_content(); ?>

  </div>
<?php }

get_footer();

?>